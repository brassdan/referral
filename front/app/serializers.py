from .models import Rlink
from rest_framework import serializers

class RlinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Rlink
        fields = ('text',)
