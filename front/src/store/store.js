// src/store/store.js

import Vue from 'vue'
import Vuex from 'vuex'
import api from './api.js'

Vue.use(Vuex)
const apiRoot = 'http://localhost:8000'  // This will change if you deploy later

const store = new Vuex.Store({
  state: {
    rlinks: []
  },
  mutations: {
    'GET_RLINKS': function (state, response) {
      state.rlinks = response.body
    },
    'ADD_RLINK': function (state, response) {
      state.rlinks.push(response.body)
    },
    'API_FAIL': function (state, error) {
      console.error(error)
    }
  },
  actions: {
    getRlinks (store) {
      return api.get(apiRoot + '/rlinks/')
        .then((response) => store.commit('GET_RLINKS', response))
        .catch((error) => store.commit('API_FAIL', error))
    },
    addRlink (store, rlink) {
      return api.post(apiRoot + '/rlinks/', rlink)
        .then((response) => store.commit('ADD_RLINK', response))
        .catch((error) => store.commit('API_FAIL', error))
    }
  }
})

export default store
