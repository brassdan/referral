from django.shortcuts import render
from django.http import HttpResponse

from rest_framework import viewsets
from rest_framework.decorators import list_route

from .models import Rlink
from .serializers import RlinkSerializer

def index(request):
    return render(request, 'index.html')

class RlinkViewSet(viewsets.ModelViewSet):
    queryset = Rlink.objects.all()
    serializer_class = RlinkSerializer

